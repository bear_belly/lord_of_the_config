#!/usr/bin/env python3

from setuptools import (
        setup,
        find_packages
    )
import os
import re

HERE = os.path.abspath(os.path.dirname(__file__))
REQ = os.path.abspath(os.path.join(HERE, 'requirements.txt'))
README = os.path.abspath(os.path.join(HERE, 'README.markdown'))

def read_requirements(path):
    if not os.path.exists(path):
        return []
    with open(path) as requirements:
        return [x.strip() for x in requirements.read().split('\n')
                if re.match(r'^[\d\w]', x)]

def get_readme(path):
    if not os.path.exists(path):
        return []
    with open(path) as readme:
        return readme.read()


setup(
        name="lotconfig",
        version="1.3",
        packages=find_packages(),
        description=get_readme(README),
        install_requires=read_requirements(REQ),
        include_package_data=True,
        author="Jordan Hewitt",
        author_email="jordan.h@startmail.com",
        license="GPLv3"
    )
